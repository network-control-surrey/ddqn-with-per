from setuptools import setup

setup(
    name='pbn_control',
    version='0.0.2',
    install_requires=[
        'git+https://github.com/vjsliogeris/gym-PBN@main#egg=gym-PBN',
        'networkx',
        'torch',
        'pyaml'
    ]
)
