# DDQN with PER

We study the ability of a Double Deep Q-Network (DDQN) with Prioritized Experience Replay (PER) in learning control strategies within a finite number of steps that drive a probabalistic Boolean network (PBN) towards a target state, typically an attractor. The control method is model-free and does not require knowledge of the network's underlying dynamics, making it suitable for applications where inference of such dynamics is intractable. We have tried the method on a number of networks, synthetic PBNs but also on PBN generated directly from real gene expression profiling data.

This project is based on the work found in our paper at: https://link.springer.com/chapter/10.1007/978-3-030-65351-4_29
 
An extended version of the above paper can be found at: https://arxiv.org/abs/1909.03331 

# Licensing 
See [LICENSE](https://gitlab.com/af00150/ddqn-with-per/-/blob/master/LICENSE) file for licensing information as it pertains to
files in this repository. 

# Environment Requirements
- CUDA 11.2+
- Python 3.9+

# Installation
*In development

# Install Environment as VM
*In development

# Documentation
*In development

# Getting Help
Principal developer: Vytenis Sliogeris

Contact E-mail: v.sliogeris@surrey.ac.uk

# Associated Publications
Deep Reinforcement Learning for Control of Probabilistic Boolean Networks

- https://link.springer.com/chapter/10.1007/978-3-030-65351-4_29

Deep Reinforcement Learning for Control of Probabilistic Boolean Networks (Extended)

- https://arxiv.org/abs/1909.03331
