from typing import List

State = List[float]  # Values are either 0 or 1
RawState = List[bool]
Attractor = List[State]
QValues = List[float]
