from typing import TypedDict, List

from .pbn import Attractor


class AgentConfig(TypedDict):
    """Agent configuration."""
    #: The PyTorch seed.
    seed: int
    #: The amount of nodes in each hidden layer of the Agent's DQN.
    height: int
    #: The amount of nodes in the input layer of the Agent's DQN.
    #  The size of the PBN in this context.
    input_size: int
    #: The amount of nodes inthe output layer of the Agent's DQN.
    #  The number of actions. Size of the PBN + 1 in this context.
    output_size: int
    #: Discount factor gamma
    gamma: float
    train_time_horizon: int


class EnvironmentConfig(TypedDict):
    """Environment configuration."""
    successful_reward: int
    action_cost: int
    wrong_attractor_cost: int
    attractors: List[Attractor]
    target_attractor: Attractor


class TrainingConfig(TypedDict):
    """Training configuration."""
    train_episodes: int
    train_epoch: int
    horizon: int
    min_epsilon: float
    decay_rate: float
    target_update: int
    memory_size: int
    batch_size: int


class GeneralConfig(TypedDict):
    """Overall application configuration."""
    agent: AgentConfig
    environment: EnvironmentConfig
    training: TrainingConfig
