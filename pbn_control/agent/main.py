"""
main.py - This module holds the actual Agent.
"""
from pbn_control.agent.types import Minibatch
import random

import torch
from torch import optim
from torch.nn import functional as F

from pbn_control.types.configs import AgentConfig, TrainingConfig
from pbn_control.types.pbn import State
from pbn_control.consts import DEVICE

from .memory import PrioritisedER, Transition
from .network import DQN


class Agent():
    """The agent of the RL algorithm. Houses the DQN, PER, etc."""
    def __init__(self, config: AgentConfig):
        """The agent of the RL algorithm. Houses the DQN, PER, etc.

        Attributes:
            input_size (int): DQN input size. Number of genes in the PBN for this case.
            device (torch.device): The devices that would be used in the DDQN.
            gamma (float): discount factor.
            train (bool): True if training, otherwise False.
            actions (list): The list containing all possible actions.
            controller (DQN): The primary DQN. The Controll network in this case.
            target (DQN): The Target DQN.

        Args:
            config (AgentConfig): the config lol
        """
        torch.manual_seed(config["seed"])
        self.device = DEVICE

        # The size of the PBN
        self.input_size = config["input_size"]
        self.actions = list(range(config["output_size"]))

        # Networks
        self.controller = DQN(config["input_size"], config["output_size"], config["height"]).to(self.device)
        self.target = DQN(config["input_size"], config["output_size"], config["height"]).to(self.device)
        self.target.load_state_dict(self.controller.state_dict())

        # Reinforcement learning parameters
        self.gamma = config["gamma"]

        # State
        self.train = False

    def load_model(self, model: DQN):
        """Load a saved model.

        Args:
            model (DQN): the saved model to load.
        """
        self.controller = model.to(self.device)
        self.target = model.to(self.device)
        self.target.load_state_dict(self.controller.state_dict())

    def _get_learned_action(self, state: State) -> int:
        with torch.no_grad():
            q_vals = self.controller(torch.tensor(state, device=self.device, dtype=torch.float))
            action = q_vals.max(0)[1].view(1, 1).item()
        return action

    def get_action(self, state: State) -> int:
        """Receive current state. Run it through DQN. If training, use epsilon greedy policy.
        Else, return the action which yields highest Q value.

        Args:
            state (State): State :)

        Returns:
            int: Integer representing the action which grants highest cumulative reward.
        """
        if self.train and random.uniform(0, 1) <= self.EPSILON:
            return random.choice(self.actions)
        else:
            return self._get_learned_action(state)

    def get_controller(self):
        # TODO Is this *really* necessary?
        return self.controller

    def _fetch_minibatch(self) -> Minibatch:
        """Fetch a minibatch from the replay memory and load it into the chosen device.

        Returns:
            Minibatch: a minibatch.
        """
        # Fetch data
        experiences, indices, weights = self.replay_memory.sample(self.batch_size, self.BETA)
        state_batch, action_batch, reward_batch, next_state_batch, dones = zip(*experiences)

        # Load to device
        state_batch = torch.tensor(state_batch, device=self.device, dtype=torch.float)\
            .view(self.batch_size, self.input_size)
        action_batch = torch.tensor(action_batch, device=self.device, dtype=torch.long).unsqueeze(1)
        reward_batch = torch.tensor(reward_batch, device=self.device, dtype=torch.float).unsqueeze(1)
        next_state_batch = torch.tensor(next_state_batch, device=self.device, dtype=torch.float)\
            .view(self.batch_size, self.input_size)
        dones = torch.tensor(dones, device=self.device, dtype=torch.float).unsqueeze(1)
        weights = torch.tensor(weights, device=self.device, dtype=torch.float).squeeze().unsqueeze(1)

        return (
            state_batch, action_batch, reward_batch, next_state_batch, dones,
            indices, weights
        )

    def feedback(self, transition: Transition):
        """Save an experience tuple, do a step of back propagation.

        Args:
            transition (Transition): Transition :)
        """
        self.replay_memory.store(transition)

        if len(self.replay_memory) >= self.batch_size:
            # Fetch data
            state_batch, action_batch, reward_batch, next_state_batch, dones,\
                 indices, weights = self._fetch_minibatch()

            # Calculate predicted actions
            with torch.no_grad():
                vals = self.controller(next_state_batch)  # TODO Wait shouldn't this be target?
                action_prime = vals.max(1)[1].unsqueeze(1)

            # Calculate current and target Q to calculate loss
            controller_Q = self.controller(state_batch).gather(1, action_batch)
            target_Q = reward_batch + (1 - dones) * self.gamma * self.target(next_state_batch).gather(1, action_prime)
            loss = F.smooth_l1_loss(controller_Q, target_Q, reduction='none')
            loss *= weights

            # Update priorities in the PER buffer
            priorities = loss + self.REPLAY_CONSTANT
            # TODO Wait don't these have to be positive? Maybe need to abs()?
            self.replay_memory.update_priorities(indices, priorities.data.detach().squeeze().cpu().numpy().tolist())

            # Back propagation
            loss = loss.mean()
            self.optimizer.zero_grad()
            loss.backward()
            for param in self.controller.parameters():
                param.grad.data.clamp_(-1, 1)
            self.optimizer.step()

        self.train_count += 1

        # Oh yeah after every TARGET_UPDATE steps the target network gets updated.
        if self.train_count % self.TARGET_UPDATE == 0:
            self.target.load_state_dict(self.controller.state_dict())

    def increment_beta(self):
        """Increment the beta exponent."""
        self.BETA = min(self.BETA + self.BETA_INCREMENT_CONSTANT, 1)

    def decrement_epsilon(self):
        """Decrement the exploration rate."""
        self.EPSILON = max(self.MIN_EPSILON, self.EPSILON - self.EPSILON_DECREMENT)

    def toggle_train(self, conf: TrainingConfig):
        """Setting all of the training params.

        Args:
            conf (TrainingConfig): the training configuration
        """
        self.train = True
        self.train_steps = conf["train_episodes"] * conf["train_epoch"]
        self.train_count = 0

        self.optimizer = optim.RMSprop(self.controller.parameters())

        # Explore-exploit
        self.EPSILON = 1
        self.MAX_EPSILON = 1
        self.MIN_EPSILON = conf["min_epsilon"]
        self.EPSILON_DECREMENT = (self.MAX_EPSILON - self.MIN_EPSILON) / self.train_steps

        # PER
        self.batch_size = conf["batch_size"]
        self.REPLAY_CONSTANT = 1e-5
        self.BETA = 0.4
        self.BETA_INCREMENT_CONSTANT = self.BETA / (0.75 * self.train_steps)
        self.replay_memory = PrioritisedER(conf["memory_size"])

        self.TARGET_UPDATE = conf["target_update"]
