"""
pbn.py - Module for handling the reward functions
"""
from typing import List, Tuple

from pbn_control.types.pbn import Attractor, RawState, State
from pbn_control.types.configs import EnvironmentConfig


def convert_state(state: RawState) -> State:
    """Encode the raw state of the PBN (list of booleans) to one that can be used in a DQN (list of integers)

    Args:
        state (RawState): The state to  "encode" (list of booleans)

    Returns:
        State: The resulting state, with booleans "encoded" into integers.
    """
    return [float(i) for i in state]


class PBNUtils():
    """Class with utilities that help sample the PBN environment."""
    def __init__(self, env_conf: EnvironmentConfig, found_attractors: List[Attractor] = None) -> None:
        """Class with utilities that help sample the PBN environment.

        Args:
            env_conf (EnvironmentConfig): Environment configuration file information
            found_attractors (List[Attractor], optional): Attractors computed from the PBN.
                Defaults to None.

        Raises:
            ValueError: If the found attractors do not correspond to the ones from the config.
            ValueError: If the target attractor isn't within the config's attractor list (i.e config invalid)
        """
        # Attractors
        self.target = env_conf["target_attractor"]
        self.conf_attractors = env_conf["attractors"]

        # Rewards
        self.successful_reward = env_conf["successful_reward"]
        self.action_cost = env_conf["action_cost"]
        self.wrong_attractor_cost = env_conf["wrong_attractor_cost"]

        # Validation
        # TODO Turn on validation!
        # if found_attractors != self.conf_attractors:
        #     raise ValueError('Attractors found in the PBN and determined in the config file do not match.')

        # if self.target not in self.conf_attractors:
        #     raise ValueError('Target attractor is not in attractor list.')

    def reward(self, next_state: List[int], action: int) -> Tuple[int, bool]:
        """Reward function. Start with reward as 0.

        Subtract `action_cost` from reward if action was taken.
        Subtract `wrong_attractor_cost` if next state is not the target attractor, but still an attractor.
        Add `successful_reward` if, *get this*, we achieve succesful control.

        Args:
            next_state (List[int]): state after an evolution step (next state)
            action (int): action taken.

        Returns:
            tuple: (reward, done)
            reward (int): reward for achieving state ns given action a.
            done (bool): Flag that indicates that control has been achieved. Used for the DDQN.
        """
        # Initial values
        done = False
        reward = 0

        # Handle win / loss condition
        if next_state in self.target:
            reward += self.successful_reward
            done = True
        else:
            attractors_matched = len([att for att in self.conf_attractors if next_state in att])
            if attractors_matched > 0:
                reward -= self.wrong_attractor_cost * attractors_matched

        # Adjust reward if action was taken
        if action != 0:
            reward -= self.action_cost

        return reward, done


# def batchTemplateF(ns, a, config):
#     """
#     Don't mind me, I don't get called.
#     """
#     done = 0
#     reward = 0
#     string = config["target_states"]
#     i = 0
#     desirable = True
#     while desirable and i < len(ns):
#         if string[i] == '*':
#             i += 1
#         elif string[i] == '1' and ns[i] == 0:
#             desirable = False
#         elif string[i] == '0' and ns[i] == 1:
#             desirable = False
#         else:
#             i += 1
#     if not desirable:
#         reward -= config["successful_reward"]
#     if a == 1:
#         reward -= config["action_cost"]
#     if desirable:
#         reward += config["successful_reward"]
#         done = 1
#     return reward, done
