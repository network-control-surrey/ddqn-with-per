#! /bin/env python

# run7our.py

# Professional libraries by real people
from pbn_control.agent.memory import Transition
import pickle
import yaml
import numpy as np

# My rubbish
from pbn_control.agent import Agent
from pbn_control.env.pbn import PBNUtils, convert_state

# Typing
from gym_PBN.envs import PBNEnv
from pbn_control.types.configs import GeneralConfig

# Loading the configuration.
conf: GeneralConfig = yaml.load(open("config_files/config_our_control_7.yml", "r"))

# Loading the environment / computing attractors
env: PBNEnv = pickle.load(open("PBN_jovanovic.pkl", "rb"))
att = env.compute_attr()
print(att)

# These are the attractors that we found in this PBN.
# I'll double check them here so that we're sure that we're working with the same PBN.
# May bin this if working with other PBNs.
# TODO Turn this validation back on.
# if att != [[[0, 1, 0, 1, 0, 0, 1]], [[1, 0, 1, 0, 1, 1, 0]], [[1, 0, 0, 1, 1, 1, 1]]]:
#     raise Exception(
#         'Attractors don\'t match the ones of PBN n=7. '
#         'If loading the PBN of n=7, contact Vytenis. Otherwise, delete these lines.'
#     )

# Computing a reward function given the configuration that we loaded and attractors we just computed.
env_helper = PBNUtils(conf["environment"], att)

agent = Agent(conf["agent"])

trainConf = conf["training"]

# Load training parameters
agent.toggle_train(trainConf)


# Keeping track of rewards/interventions
# Epoch, episode, value
rewards = np.zeros((trainConf["train_epoch"], trainConf["train_episodes"]), dtype=float)
interventions = np.zeros((trainConf["train_epoch"], trainConf["train_episodes"]), dtype=float)


# Training loop. Go figure.
for epoch in range(trainConf["train_epoch"]):

    for episode in range(trainConf["train_episodes"]):
        env.reset()  # Resetting to a random state.
        old_state = list(env.render())
        attractor_found = 0

        control_steps = 0
        r_sub = 0

        while not attractor_found and control_steps < trainConf["horizon"]:
            action = agent.get_action(convert_state(old_state))
            env.step(action)
            next_state = list(env.render())
            control_steps += 1
            reward, attractor_found = env_helper.reward(next_state, action)
            agent.feedback(
                Transition(convert_state(old_state), action, reward, convert_state(next_state), attractor_found)
            )
            old_state = next_state
            r_sub += reward

        agent.increment_beta()
        agent.decrement_epsilon()

        rewards[epoch, episode] = r_sub
        interventions[epoch, episode] = control_steps

    print(
        f"Epoch: {epoch}, "
        f"R: {np.mean(rewards, axis=1)[epoch]}, "
        f"intervention: {np.mean(interventions, axis=1)[epoch]}, "
        f"E: {agent.EPSILON}, B: {agent.BETA}"
    )

    with open("rewards.pkl", "wb") as f:
        pickle.dump(rewards, f)
    with open("interventions.pkl", "wb") as f:
        pickle.dump(interventions, f)
    with open("network.pkl", "wb") as f:
        pickle.dump(agent.get_controller(), f)


env.close()
